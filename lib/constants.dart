import 'package:flutter/material.dart';

const Color primaryColor = Color(0xFFB90404);
const Color backgroundColor = Color(0xDD161B22);
const Color fontColor = Color(0xFFFFFFFF);
